/*
Default values of public properties:
allExceptFilter = new Array("A.*", 
                            "BUTTON.*", 
                            "IMG.*", 
                            "INPUT.*", 
                            "OBJECT.*", 
                            "OPTION.*", 
                            "SELECT.*", 
                            "TEXTAREA.*");
noneExceptFilter = new Array();
menuClass = "jsdomenudiv";
itemClass = "jsdomenuitem";
itemClassOver = "jsdomenuitemover";
sepClass = "jsdomenusep";
imgSrc = "arrow.png";
imgSrcOver = "arrow_o.png";
imgOffset = 5;
menuBorderWidth = 2;
menuDelay = 0;
*/

// Uncomment any of the following public properties to override the default value.
//var allExceptFilter = 
//var noneExceptFilter = 
//var menuClass = 
//var itemClass = 
//var itemClassOver = 
//var sepClass = 
var imgSrc = "modules/jsdomenu/lib/alt_arrow.png";
var imgSrcOver = "modules/jsdomenu/lib/alt_arrow_o.png";
//var imgOffset = 
var menuBorderWidth = 1;
//var menuDelay = 
