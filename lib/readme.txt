jsDOMenu: Readme

/*

    jsDOMenu: A cross-browser Javascript navigation menu that supports 
    unlimited submenus and is fully customizable.

    Version: 1.1.3
    Released on: 30 December 2003
    Copyright (C) 2003  Toh Zhiqiang
    Web Site: http://www.tohzhiqiang.per.sg/projects/jsdomenu/

    This script is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This script is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

Project: jsDOMenu
Development Status: Beta/Stable
Version: 1.1.3 (view changelog.txt for more information)
License: GNU General Public License (View license.txt for more information)
Programming Language: Javascript
Requirement: DOM-compatible browser such as Internet Explorer 6, Netscape 7, Mozilla Firebird and Opera

Description
A cross-browser navigation menu that is based on DOM. The menu will pop up when you left/right click in certain regions in your web page. You can also configure it to be at a fixed point in your web page, or move with scrolling. It supports unlimited submenus, and submenu will adjust its position appropriately if the cursor is too near the edge. Menu is 100% HTML/XHTML valid, and is fully customizable through Javascript/CSS. Tested in Internet Explorer 5 (SP2), Internet Explorer 5.5 (SP2), Internet Explorer 6, Netscape 7.1, Mozilla Firebird 0.7, and Opera 7.23.

Features
Some of the features of jsDOMenu are outlined below: 

~ Tested and works in different browsers such as Internet Explorer 5+, Netscape 7.1, Mozilla Firebird 0.7, and Opera 7.23.

~ Supports unlimited submenus.

~ Smart placement of menus and submenus. The menu will pop up at different place if the cursor is positioned too close to the edge, e.g. appears on the cursor's left if the cursor is too close to the right edge.

~ Font, colour, background colour, styles, etc, of menus and menu items are fully customizable through Javascript/CSS. 
~ Totally HTML\XHTML-compliant. You do not need to insert any additional HTML tags such as <div>. No document.write() or document.writeln() used.

~ Configure whether the menu should show/hide on which mouse click, always visible at a fixed point in the web page, or move with scrolling.

~ Released under GNU GPL. You can help to improve jsDOMenu so that it works in more browsers! 

Note: The Webmaster will appreciate that you inform him whether jsDOMenu works/does not work in other platforms/browsers. For example, does jsDOMenu work in Camino?

Restrictions
Some of the restrictions of jsDOMenu are outlined below: 

~ Only works in web pages without frames. 

Documentation
View the Documentation page (http://www.tohzhiqiang.per.sg/projects/jsdomenu/docs/) to learn how to install jsDOMenu and create the menus using simple Javascript codes. 

Note: If you can think of any better method/implementation, please kindly inform the Webmaster so that he can include it in the next release of jsDOMenu.

Download
The latest version of jsDOMenu can be downloaded at http://www.tohzhiqiang.per.sg/projects/jsdomenu/.

Contact
Any bug to report, question, problem, suggestion, complaint, etc, please contact Webmaster: webmaster@tohzhiqiang.per.sg