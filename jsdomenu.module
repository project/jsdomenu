<?php

/**
 * @file
 * Enables popup menus, including a block for navigation.
 */

/** Get the list of available themes
 */
function jsdomenu_available_themes() {
  if ($handle = opendir('modules/jsdomenu/lib/themes')) {
    while (false !== ($file = readdir($handle))) {
      if ($file != "." && $file != ".." && is_dir("modules/jsdomenu/lib/themes/$file")) {
           $jsdomenu_themes["modules/jsdomenu/lib/themes/$file"] = $file;
       }
   }
   closedir($handle);
   return $jsdomenu_themes;
  }
}
/**
 * Implementation of hook_settings().
 */
function jsdomenu_settings() {
  $vocabs = array(0 => "none");
  $vocabularies = taxonomy_get_vocabularies();
  foreach ($vocabularies as $vocabulary) {
    $vocabs[$vocabulary->vid] = $vocabulary->name;
  }
  $output .= form_select(t('jsdomenu style'), 'jsdomenu_css', variable_get('jsdomenu_css', 'default'), jsdomenu_available_themes(), t('Choose the menu theme.'));
  $output .= form_select(t("Menu placement"), "jsdomenu_placement", variable_get("jsdomenu_placement", 'absolute'), array('absolute' => t('Compressed in jsDoMenu block'), 'static' => t('Visible in jsDoMenu block'), 'cursor' => t('Appears at cursor position on click')), t("Placement of the menu on the page)."));
  $output .= form_select(t("Menu vocabulary"), "jsdomenu_navigation_vocab", variable_get("jsdomenu_navigation_vocab", 1), $vocabs, t("Vocabulary from which navigation links will be generated (one link for each first-level term in vocabulary)."));
  $output .= form_textfield(t("Block title"), 'jsdomenu_block_title', variable_get("jsdomenu_block_title", 'Shortcuts'), 30, 30, t("This title will be the block heading if a placement other than cursor positioning is used"), NULL, TRUE);
  $output .= form_textfield(t("Block instructions"), 'jsdomenu_block_instructions', variable_get("jsdomenu_block_instructions", 'Click here for menu'), 30, 30, t("These instructions will appear in the block, and MUST be present if you select the placement mode 'Compressed in jsDoMenu block"), NULL, TRUE);
  $output .= form_checkbox("Nodes included in menu", 'jsdomenu_navigation_nodes', TRUE, variable_get('jsdomenu_navigation_nodes', FALSE), "Check this box if you want to return nodes in the menus as well as taxonomy terms");
  return $output;
}

/**
 * Implementation of hook_onload().
 */
function jsdomenu_onload() {
  return array("initjsDOMenu()");
}

/**
 * Implementation of hook_menu().
 */
function jsdomenu_menu($may_cache) {
  if ($may_cache) {
    jsdomenu_set_head();
  }
}

/**
 * Get the css file corresponding to the current theme
 */
function jsdomenu_get_theme($theme) {
  $css = glob("$theme/*.css");
  $theme_config['css'] = $css[0];
  if (file_exists("modules/jsdomenu/lib/jsdomenu.config.js")) {
    $theme_config['config'] = "modules/jsdomenu/lib/jsdomenu.config.js";
    }
  else {
    $theme_config['config'] = "$theme/jsdomenu.config.js";
    }
  return $theme_config;


}

/**
 * Add script elements to HTML head.
 */
function jsdomenu_set_head() {
  global $base_url;
  $theme_config = jsdomenu_get_theme(variable_get("jsdomenu_css", "modules/jsdomenu/lib/themes/classic"));
      $output .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"".$base_url."/".$theme_config['css']."\" />\n";
      $output .= "<script type=\"text/javascript\" src=\"".$base_url."/".$theme_config['config']."\"></script>\n";

  $output .= "<script type=\"text/javascript\" src=\"".$base_url."/modules/jsdomenu/lib/jsdomenu.js\"></script>\n";
  drupal_set_html_head($output);

}
/**
 * Implementation of hook_block().
 */
function jsdomenu_block($op = "list", $delta = 0) {
  $content = '';
  if($op == "list") {
    $blocks[]["info"] = "jsdomenu dynamic menu";
    return $blocks;
  }
  else {
    if (variable_get("jsdomenu_placement", 'absolute') != 'cursor') {
      $block["subject"] = variable_get("jsdomenu_block_title", 'Shortcuts');
      }
    $vid = variable_get("jsdomenu_navigation_vocab", 1);
    $tree = taxonomy_get_tree($vid);
    if (variable_get("jsdomenu_placement", 'absolute') == 'absolute') {
      $content = "<div id=\"showup\">" . variable_get("jsdomenu_block_instructions", 'Click here for menu') . "</div>\n";
      }
    else {
      $content = "<div id=\"showup\"></div>\n";
      }
    $content .= jsdomenu_make("term", $tree);
    $block["content"] = $content;
    return $block;
  }
}

/**
 * Construct menu calls.
 */
function jsdomenu_make($type = "term", $tree) {
  $depth = -1;
  $gen[$depth] = "main";
  $output .= "<script type=\"text/javascript\">\n";
  $output .= "function createjsDOMenu() {\n";
  switch(variable_get("jsdomenu_placement", 'absolute')) {
    case 'static':
      $output .= "  menu_main = new jsDOMenu(130, \"static\", \"showup\", true);\n";
      break;
    case 'absolute':
      $output .= "  menu_main = new jsDOMenu(130, \"absolute\", \"\", false);\n";
      break;
    case 'cursor':
      $output .= "  menu_main = new jsDOMenu(130, \"cursor\", \"\", false);\n";
      break;
  }
  foreach ($tree as $item) {
    switch($type) {
      case "term":
        $item->id = $item->tid;
        $item->url = url("taxonomy/term/$item->tid",NULL,NULL,TRUE);
    }
    $gen[$item->depth] = $item->id;
    if(($item->depth != 0) && ($item->depth > $depth)) {
      $output .= "  menu_" . $gen[$item->depth -1] . " =  new jsDOMenu(140, \"absolute\");\n";
      $output .= "  menu_" . $gen[$item->depth - 2]. ".items.item_" . $gen[$item->depth - 1] . ".setSubMenu(menu_" . $gen[$item->depth -1] . ");\n";
      }
    $output .= "  menu_" . $gen[$item->depth - 1] . ".addMenuItem(new menuItem(\"" . $item->name . "\", \"item_" . $item->id . "\", \"" . $item->url . "\"))\n";
    $depth = $item->depth;
// Stuff for adding node level menus
    if(variable_get("jsdomenu_navigation_nodes", FALSE) && taxonomy_term_count_nodes($item->id) > 0) {
      $nodes = taxonomy_select_nodes(array($item->id), '', 1, FALSE);
      $output .= "  menu_" . $item->id . "_nodes =  new jsDOMenu(250, \"absolute\");\n";
      $output .= "  menu_" . $gen[$item->depth - 1]. ".items.item_" . $item->id . ".setSubMenu(menu_" . $item->id . "_nodes);\n";
      while ($n = db_fetch_object($nodes)) {
        $result = db_query("select title from {node} where nid = $n->nid");
        $node = db_fetch_object($result);
        $output .= "  menu_" . $item->id . "_nodes.addMenuItem(new menuItem(\"" . $node->title . "\", \"item_" . $n->nid . "\", \"node/" . $n->nid . "\"))\n";
      }
    }
  }

    switch(variable_get("jsdomenu_placement", 'absolute')) {
      case 'static':
	   $output .= "  menu_main.show();";
	   break;
      case 'absolute':
       $output .= "  menu_main.setNoneExceptFilter(new Array(\"div.showup\"));";
       $output .= "  setPopUpMenu(menu_main);";
       $output .= "  activatePopUpMenuBy(0, 0);";
       break;
      case 'cursor':
       $output .= "  setPopUpMenu(menu_main);";
       $output .= "  activatePopUpMenuBy(0, 0);";
       break;
    }
  $output .= "}\n";
  $output .= "</script>\n";
  return $output;
}


/**
 * Implementation of hook_help().
 */
function jsdomenu_help($section = "admin/help#jsdomenu") {
  switch ($section) {
    case 'admin/help#jsdomenu':
      return t("<p>This module can be used to display dynamic menus. It has three different modes of operation:
      <ul><li>Compressed: (compatible with previous version) the jsDoMenu block displays a message 'click here for menu', clicking on the message displays the toplevel menu;</li>
      <li>Visible: the toplevel menu is visible in the jsDoMenu block;</li>
      <li>Cursor: the menu appears whenever the left mouse key is pressed on the page (it is recommended not to activate the block)</li></ul></p>
      <p>In addition, it is possible to choose whether nodes should appear in the menus, or whether they should be limited to displaying taxonomies</p>
      <p>By default, jsDoMenu is installed with two menu themes: 'classic' and 'XP Office'. To create different themes, create a new directory under 'themes', and modify one of the default themes as you please. It is also possible to alter the configuration file and make it specific to a theme by copying it into the appropriate directory");
    case 'admin/modules#description':
      return t("A utility module to generate dynamic menus.");
  }
}
?>
