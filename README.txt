jsdomenu.module
README

Description
------------

The jsdomenu module enables the use of popup menus using the jsDOMenu
library developed by Toh Zhiqiang.

The functions use DOM methods and are therefore limited to more recent
browsers (IE5+, NS6+, Moz 1+).

To date the module includes a dynamically-generated menu showing terms
from a taxonomy vocabulary, with associated nodes (optional).


